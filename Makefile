CFLAGS = -fPIC -DSYSLOG_NAMES -D_GNU_SOURCE -Werror -Wall -Wextra
LDFLAGS = -ldl

LIB_NAME = minisyslog.so
OBJS = minisyslog.o

all: $(LIB_NAME)

$(LIB_NAME): $(OBJS) Makefile
	$(CC) -shared $(CLFAGS) $(LDFLAGS) -o '$@' $(OBJS)

clean:
	$(RM) $(OBJS)

distclean: clean
	$(RM) "$(LIB_NAME)"

.PHONY: all clean distclean
.INTERMEDIATE: $(OBJS)
