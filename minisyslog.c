#include <stdarg.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <limits.h>
#include <err.h>
#include <time.h>
#include <unistd.h>
#include <syslog.h>

#define MAX_ENV_LENGTH 256
#define DEFAULT_OUTPUT_FILE "/dev/stderr";
#define DEFAULT_TIME_FORMAT "%c"

static char output_file_path[MAX_ENV_LENGTH + 1];
static char time_format[MAX_ENV_LENGTH + 1];
static FILE *logfile = NULL;
static const char *logident = NULL;
static char hostname[HOST_NAME_MAX];

void __vsyslog_chk(int priority, int flag, const char *format, va_list ap);

void openlog(const char *ident, int option, int facility)
{
	(void) option;
	(void) facility;

	logident = ident ? ident : program_invocation_short_name;
	gethostname(hostname, sizeof (hostname));

	if (!(logfile = fopen(output_file_path, "a")))
		warn("minisyslog: unable to open '%s'!", output_file_path);
}

void syslog(int priority, const char *format, ...)
{
	va_list ap;
	va_start(ap, format);

	__vsyslog_chk(priority, -1, format, ap);

	va_end(ap);
}

void __syslog_chk(int priority, int flag, const char *format, ...)
{
	va_list ap;
	va_start(ap, format);

	__vsyslog_chk(priority, flag, format, ap);

	va_end(ap);
}

void vsyslog(int priority, const char *format, va_list ap)
{
	__vsyslog_chk(priority, -1, format, ap);
}

void __vsyslog_chk(int priority, int flag, const char *format, va_list ap)
{
	char date_buffer[64];
	time_t t;
	const struct tm *tm_info;
	int logmask;

	(void) priority;

	logmask = setlogmask(0);

	if (!(LOG_MASK(LOG_PRI(priority)) & logmask))
		return;

	if (!logfile)
		openlog(NULL, 0, LOG_USER);
	if (!logfile)
		return;

	t = time(NULL);
	tm_info = localtime(&t);
	if (!strftime(date_buffer, sizeof (date_buffer), time_format, tm_info))
		date_buffer[0] = '\0';

	fprintf(logfile, "%s %s %s[%jd]: ", date_buffer, hostname, logident,
	                                    (intmax_t) getpid());
	if (flag == -1)
		vfprintf(logfile, format, ap);
	else
		__vfprintf_chk(logfile, flag, format, ap);
	fputc('\n', logfile);
}

void closelog(void)
{
	if (logfile) {
		fclose(logfile);
		logfile = NULL;
	}
}

__attribute__((constructor)) static void _init(void)
{
	const char *value;

        if (!(value = getenv("MINISYSLOG_OUTPUT_FILE")))
		value = DEFAULT_OUTPUT_FILE;
	strncpy(output_file_path, value, MAX_ENV_LENGTH);
	output_file_path[MAX_ENV_LENGTH] = '\0';


        if (!(value = getenv("MINISYSLOG_TIME_FORMAT")))
		value = DEFAULT_TIME_FORMAT;
	strncpy(time_format, value, MAX_ENV_LENGTH - 1);
	time_format[MAX_ENV_LENGTH] = '\0';
}
