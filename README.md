MINISYSLOG
===========

Usage
-----

This library is intended to be used with `LD_PRELOAD` to make every program
using syslog to send its messages to a specific log file, set by the
environment.

You may add the path to this library in `/etc/ld.so/preload` to make it work
globally without having to set the `LD_PRELOAD` environment variable.

The following environment variables can be set:
  - `MINISYSLOG_OUTPUT_FILE` is the file where the logs are writtent to.
  - `MINISYSLOG_TIME_FORMAT` is a `strftime(3)` format string for customizing
    timestamps in logs.

Usage with Docker
-----------------

You may add the following instructions to your Dockerfile

    COPY --from=registry.cri.epita.fr/cri/docker/minisyslog /minisyslog/minisyslog.so /usr/local/lib/
    RUN echo "/usr/local/lib/minisyslog.so" >> /etc/ld.so.preload
    ENV MINISYSLOG_OUTPUT_FILE /dev/stdout
